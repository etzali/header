class SessionsController < ApplicationController

  def new
    if current_user
      redirect_to root_path, notice: 'Welcome ' + current_user.email + '!'
    end
  end

  def create
    user = User.find_by_email params[:email]

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_path, notice: 'Logged In'
    else
      flash.now[:alert] = 'Email or Password is invalid'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'Logged out!'
  end
end
