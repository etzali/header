# Taken from Link Up West Dunbartonshire

 module Rescuer
  extend ActiveSupport::Concern

  included do
    rescue_from Pundit::NotAuthorizedError, with: :not_authorized
    rescue_from Pundit::NotDefinedError, with: :not_found
    rescue_from ActiveRecord::RecordNotFound, with: :not_found
    rescue_from ActionController::RoutingError, with: :not_found
    rescue_from ActionController::UnknownFormat, with: :not_found
  end

  def raise_routing_error
    raise ActionController::RoutingError, request.original_url
  end

  private

    def not_authorized(exception)
      policy_name = exception.policy.class.to_s.underscore
      alert = t "#{policy_name}.#{exception.query}", scope: 'pundit', default: :default
      flash.now[:alert] = alert
      render template: 'errors/not_authorized', status: :forbidden, layout: 'application', formats: :html
    end

    def not_found
      render template: 'errors/not_found', status: :not_found, layout: 'application', formats: :html
    end
end
