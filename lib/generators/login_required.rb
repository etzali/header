 module LoginRequired
  extend ActiveSupport::Concern

  def login_required
    redirect_path = login_url(redirect_to: request.path)
    redirect_to(redirect_path, alert: 'Please login first...') unless current_user.present?
  end

  def after_login
    params[:redirect_to] || user_path(current_user)
  end
end
