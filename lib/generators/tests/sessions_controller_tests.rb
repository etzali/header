require "test_helper"

class SessionsControllerTest < ActionDispatch::IntegrationTest

  # NEW

  test "should get new" do
    get login_url
    assert_response :success
  end

  test "should get new if already logged in" do
    user = users(:one)
    login user
    get login_url
    assert_redirected_to root_path
    assert_equal "Welcome #{user.email}!", flash[:notice]
  end

  # CREATE

  test "create should go to root and set user id" do
    user = users(:one)
    post create_login_url email: user.email, password: "one"
    assert_redirected_to root_path
    assert_equal "Logged In", flash[:notice]
    assert_equal user.id, session[:user_id]
  end

  test "create should fail if password wrong" do
    user = users(:one)
    post create_login_url email: user.email, password: ""
    assert_equal login_path, path
    assert_select "p", "Email or Password is invalid"
    assert_nil session[:user_id]
  end

  test "create should fail if email wrong" do
    user = users(:one)
    post create_login_url email: "", password: "secret"
    assert_equal login_path, path
    assert_select "p", "Email or Password is invalid"
    assert_nil session[:user_id]
  end

  # DESTROY

  test "should get destroy" do
    get logout_url
    assert_redirected_to root_url
    assert_equal "Logged out!", flash[:notice]
    assert_nil session[:user_id]
  end

  test "logout should work if logged in" do
    login users(:one)
    get logout_url
    assert_redirected_to root_url
    assert_equal "Logged out!", flash[:notice]
    assert_nil session[:user_id]
  end

end
