require 'test_helper'

class UserTest < ActiveSupport::TestCase

  setup do
    @user = users(:one)
  end

  # VALIDATIONS

  test 'user must have email' do
    assert_no_difference('User.count') do
      User.create email: '', password: 'password', password_confirmation: 'password'
    end
  end

  test 'email must be unique' do
    assert_no_difference('User.count') do
      user = User.create email: @user.email, password: 'password', password_confirmation: 'password'
      refute user.valid?
      assert_equal user.errors.count, 1
      assert_includes user.errors.messages[:email], 'has already been taken'
    end
  end

end
